
    <CombinationFinder identifies 4-hit combinations of carcinogenic genes.>
    Copyright (C) <2020>  <CombinationFinderDevs>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.



# Scaling out a Combinatorial Algorithm for Discovering Carcinogenic Gene Combinations #

We have designed a combinatorial algorithm to identify carcinogenic gene combinations for 31 cancer types from TCGA mutation data.
Since the runtime for this algorithm grows exponentially with the length of the combination

### Source Code ###

* compute_4hits.cu
* a compiler script (compile_script.sh) is included to compile the code to an executable
* ./compile_script.sh compute_4hits_2x2.cu compute_4hits_2x2.o (2x2 scheme) OR
* ./compile_script.sh compute_4hits_3x1.cu compute_4hits_3x1.o (3x1 scheme)

### Computing 4-hit combinations ###

* bsub ACC-100-2x2.lsf OR bsub ACC-100-3x1.lsf
* jsrun -n100 -a1 -c1 -g6 --bind=proportional-packed:1 --launch_distribution=packed stdbuf -o0 ./compute_4hits_2x2.o (OR ./compute_4hits_3x1.o) $data_dir/ACC.maf2dat.matrix.out.training $data_dir/manifest_normal_normal.txt.training.txt.geneSampleList 0.1 6 ACC-100-schedule-2x2.txt (OR ACC-100-schedule-3x1.txt) 100


### Data ###

* Tumor samples data: data/ACC.maf2dat.matrix.out.training, $data_dir/ACC.maf2dat.matrix.out.test
* Normal samples data: data/manifest_normal_normal.txt.training.txt.geneSampleList, data/manifest_normal_normal.txt.test.txt.geneSampleList

### Results ###

* Results.xlsx has 3 workbooks with following 3 results
* Classification performance
* 314 combinations
* Speedup on 100 Summit nodes